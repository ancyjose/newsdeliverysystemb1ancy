import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

class Main_Agent_Class {

	public static void main(String[] args) {
		GUI gui = new GUI();
		/*
		systemDB system = new systemDB();
		Customer customer = new Customer();
		system.connectToGUI(gui);
		gui.connectToDB(system);
		customer.connectToGUI(gui);
		gui.connectToCustomer(customer);
		customer.connectToDB(system); */
	}

}

class GUI {
	private String password = "U2";
	private boolean access = false;
	//private systemDB DB;
	//private Customer customer;

	public GUI() {
		JFrame frame = new JFrame();
		frame.setTitle("");

		Container cp = frame.getContentPane();

		Font f = new Font("ALGERIAN", Font.ITALIC | Font.BOLD, 26);

		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBorder(BorderFactory.createTitledBorder("  NEWS Agency System   "));
		JPanel top = new JPanel();
		top.setPreferredSize(new Dimension(800, 300));
		top.setBackground(Color.blue);
		top.setLayout(new BorderLayout());
		top.setBorder(new EmptyBorder(15, 30, 10, 30));
		JPanel left = new JPanel();
		left.setBackground(Color.blue);
		left.setBorder(new EmptyBorder(0, 10, 0, 10));
		JPanel mid = new JPanel();
		JPanel right = new JPanel();
		right.setBackground(Color.blue);
		right.setBorder(new EmptyBorder(0, 10, 0, 10));
		JPanel bot = new JPanel();
		bot.setPreferredSize(new Dimension(800, 80));
		bot.setBackground(Color.blue);

		JTextArea logs = new JTextArea(20, 20);
		JScrollPane scrollLogs = new JScrollPane(logs);
		top.add(scrollLogs, BorderLayout.CENTER);

		CardLayout cards = new CardLayout();
		mid.setLayout(cards);

		JPanel login = new JPanel();
		JPanel custAcc = new JPanel();
		JPanel deliveries = new JPanel();
		JPanel subscriptions = new JPanel();

		mid.add(login, "login");
		mid.add(custAcc, "customer");
		mid.add(deliveries, "deliveries");
		mid.add(subscriptions, "subscriptions");
		

		/************************************************************************************************************************************/
		/*********************************************  LOGIN CARD **************************************************************************/
		login.setLayout(new GridLayout(5, 3));
		login.setBorder(BorderFactory.createTitledBorder("  LOGIN   "));
		login.setBackground(Color.cyan);
		JButton manager = new JButton("System Access");
		manager.setFont(f);
		manager.setForeground(Color.RED);
		JButton delivery = new JButton("Deliveries");
		delivery.setFont(f);
		JButton subscription = new JButton("Subscription");
		subscription.setFont(f);
		subscription.setForeground(Color.BLACK);
		final Color dark_green = new Color(0, 153, 0);
		delivery.setForeground(dark_green);
		for (int i = 0; i < 15; i++) {
			if (i == 4) {
				login.add(manager);
			} else if (i == 10) {
				login.add(delivery);
			} else if (i == 13) {
				login.add(subscription);
			}else {
				login.add(Box.createHorizontalGlue());
			}
		}

		/************************************************************************************************************************************/
		/*********************************************  CUSTOMER CARD ***********************************************************************/
		custAcc.setBackground(Color.orange);
		custAcc.setBorder(BorderFactory.createTitledBorder("  CUSTOMER   "));

		JRadioButton cre = new JRadioButton("  Create new Account  ");
		cre.setOpaque(false);
		cre.setSelected(true);
		JRadioButton arch = new JRadioButton("  Archive Account  ");
		arch.setOpaque(false);
		JRadioButton sea = new JRadioButton("  Search Account  ");
		sea.setOpaque(false);
		JButton archB = new JButton("Archive Account");
		Box b1 = new Box(BoxLayout.Y_AXIS);
		JPanel b1P = new JPanel();
		b1P.setBackground(Color.red);
		b1P.setBorder(BorderFactory.createTitledBorder("  Options   "));
		b1P.add(b1);
		b1.add(cre);
		b1.add(sea);
		b1.add(arch);
		b1.add(archB);
		ButtonGroup bg1 = new ButtonGroup();
		bg1.add(cre);
		bg1.add(arch);
		bg1.add(sea);

		JLabel firstName = new JLabel("First Name:");
		JLabel lastName = new JLabel("Last Name:");
		JLabel dateOfBirth = new JLabel("Year of birth:  ");
		JLabel addressStreet = new JLabel("Address:");
		JLabel phone = new JLabel("Phone No.:");
		JLabel email = new JLabel("E-mail:");
		JLabel areaCode = new JLabel("Area Code:");
		JLabel custId = new JLabel("Customer ID:  ");
		JLabel subsId = new JLabel("Subscrip. ID:  ");
		JPanel b3 = new JPanel();
		b3.setBackground(Color.orange);
		b3.setLayout(new GridLayout(8, 1, 0, 5));
		b3.setBorder(BorderFactory.createEmptyBorder(20, 80, 0, 0));
		b3.add(firstName);
		b3.add(lastName);
		b3.add(dateOfBirth);
		b3.add(addressStreet);
		b3.add(phone);
		b3.add(email);
		b3.add(areaCode);

		JTextField fNtxt = new JTextField(15);
		JTextField lNtxt = new JTextField(15);
		JTextField dOBtxt = new JTextField(15);
		JTextField aStxt = new JTextField(10);
		JTextField ptxt = new JTextField("0000000000", 15);
		JTextField etxt = new JTextField(15);
		JTextField aCtxt = new JTextField(15);
		JTextField cIdtxt = new JTextField(15);
		JTextField sIdtxt = new JTextField(15);
		JPanel b4 = new JPanel();
		b4.setBackground(Color.orange);
		b4.setLayout(new GridLayout(8, 1, 0, 5));
		b4.setBorder(BorderFactory.createEmptyBorder(20, 0, 0, 80));
		b4.add(fNtxt);
		b4.add(lNtxt);
		b4.add(dOBtxt);
		b4.add(aStxt);
		b4.add(ptxt);
		b4.add(etxt);
		b4.add(aCtxt);

		Box b5 = new Box(BoxLayout.X_AXIS);
		b5.add(b1P);
		b5.add(b3);
		b5.add(b4);

		JButton creB = new JButton("Create Account");
		JButton modB = new JButton("Modify Account");
		JPanel b6 = new JPanel();
		b6.setBackground(Color.orange);
		b6.setLayout(new GridLayout(2, 1, 50, 0));
		b6.add(modB);
		b6.add(creB);

		JCheckBox nm = new JCheckBox("Name");
		nm.setOpaque(false);
		JCheckBox add = new JCheckBox("Cust. ID");
		add.setOpaque(false);
		JCheckBox cusId = new JCheckBox("Address");
		cusId.setOpaque(false);
		JCheckBox subId = new JCheckBox("Subs. ID");
		subId.setOpaque(false);
		JButton searchB = new JButton("Search Account");
		Box b2 = new Box(BoxLayout.Y_AXIS);
		b2.add(nm);
		b2.add(add);
		b2.add(cusId);
		b2.add(subId);
		b2.add(searchB);
		JPanel b2P = new JPanel();
		b2P.setBackground(Color.red);
		b2P.setBorder(BorderFactory.createTitledBorder(" Search by:  "));
		b2P.add(b2);
		ButtonGroup bg2 = new ButtonGroup();
		bg2.add(nm);
		bg2.add(add);
		bg2.add(cusId);
		bg2.add(subId);

		JComboBox customersFound = new JComboBox();
		customersFound.setModel(
				new DefaultComboBoxModel<String>(new String[] { " List of Customers found in the Data Base " }));

		Box b7 = new Box(BoxLayout.Y_AXIS);
		b7.add(b6);
		b7.add(b2P);

		Box b8 = new Box(BoxLayout.X_AXIS);
		b8.add(b5);
		b8.add(b7);
		Box b9 = new Box(BoxLayout.Y_AXIS);
		b9.add(customersFound);
		b9.add(b8);

		custAcc.add(b9);
		
		
		/************************************************************************************************************************************/
		/********************************************* DELIVERY CARD ************************************************************************/
		deliveries.setBackground(Color.green);
		deliveries.setBorder(BorderFactory.createTitledBorder(" DELIVERIES "));

		// cards.show(mid, "customer");

		modB.setVisible(false);
		archB.setVisible(false);
		customersFound.setVisible(false);
		b2P.setVisible(false);
		ActionListener radioButtonEvent = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == cre) {
					creB.setVisible(true);
					modB.setVisible(false);
					archB.setVisible(false);
					customersFound.setVisible(false);
					b2P.setVisible(false);

					b3.removeAll();
					b4.removeAll();

					b3.add(firstName);
					b3.add(lastName);
					b3.add(dateOfBirth);
					b3.add(addressStreet);
					b3.add(phone);
					b3.add(email);
					b3.add(areaCode);

					b4.add(fNtxt);
					b4.add(lNtxt);
					b4.add(dOBtxt);
					b4.add(aStxt);
					b4.add(ptxt);
					b4.add(etxt);
					b4.add(aCtxt);

					b3.revalidate();
					b3.repaint();
					b4.revalidate();
					b4.repaint();

				} else {
					if (e.getSource() == sea) {
						creB.setVisible(false);
						modB.setVisible(true);
						archB.setVisible(false);
						customersFound.setVisible(true);
						b2P.setVisible(true);

					}
					if (e.getSource() == arch) {
						creB.setVisible(false);
						modB.setVisible(true);
						archB.setVisible(true);
						customersFound.setVisible(true);
						b2P.setVisible(true);
					}
					b3.remove(firstName);
					b3.remove(dateOfBirth);
					b3.remove(phone);
					b3.remove(email);
					b3.remove(areaCode);
					b3.add(custId);
					b3.add(subsId);

					b4.remove(fNtxt);
					b4.remove(dOBtxt);
					b4.remove(ptxt);
					b4.remove(etxt);
					b4.remove(aCtxt);
					b4.add(cIdtxt);
					b4.add(sIdtxt);

					b3.revalidate();
					b3.repaint();
					b4.revalidate();
					b4.repaint();
				}
			}
		};
		cre.addActionListener(radioButtonEvent);
		sea.addActionListener(radioButtonEvent);
		arch.addActionListener(radioButtonEvent);

		ActionListener LoginButtonsEvent = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Object target = e.getSource();
				if (target == manager) {
					JTextField pwd = new JPasswordField(10);
					int result = JOptionPane.showConfirmDialog(null, pwd, "Please input the Access password",
							JOptionPane.OK_CANCEL_OPTION);
					if (result == JOptionPane.OK_OPTION) {
						String pass = pwd.getText();
						if (pass.equals(password)) {
							cards.show(mid, "customer");
						} else {
							JOptionPane.showMessageDialog(frame, "Sorry, Access Denied...", "Password ERROR",
									JOptionPane.ERROR_MESSAGE);
						}
					}
				}
				if (target == delivery) {
					cards.show(mid, "deliveries");
				}
				
				if (target == subscription) {
					cards.show(mid, "subscriptions");
					
				}

			}
		};
		manager.addActionListener(LoginButtonsEvent);
		delivery.addActionListener(LoginButtonsEvent);
		subscription.addActionListener(LoginButtonsEvent);

		ActionListener CustomerButtonsEvent = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Object target = e.getSource();
				if (target == creB) {
					String fN = fNtxt.getText();
					String lN = lNtxt.getText();
					int dOB = Integer.valueOf(dOBtxt.getText());
					String aS = aStxt.getText();
					int p = Integer.valueOf(ptxt.getText());
					String em = etxt.getText();
					int a = Integer.valueOf(aCtxt.getText());

					// send to Kalos's method for check
					// object.method(fN, lN, dOB, aS, p, em, a);
				}
				if (target == searchB) {
					String choice1 = "";
					int choice2 = 0;
					if (nm.isSelected()) {
						choice1 = lNtxt.getText();
					}
					if (add.isSelected()) {
						choice1 = aStxt.getText();
					}
					if (cusId.isSelected()) {
						choice2 = Integer.valueOf(cIdtxt.getText());
					}
					if (subId.isSelected()) {
						choice2 = Integer.valueOf(sIdtxt.getText());
					}
					
					// Send to Kalos object.method(choice1, choice2);
				}
				if(target == modB){
					String fN = fNtxt.getText();
					String lN = lNtxt.getText();
					int dOB = Integer.valueOf(dOBtxt.getText());
					String aS = aStxt.getText();
					int p = Integer.valueOf(ptxt.getText());
					String em = etxt.getText();
					int a = Integer.valueOf(aCtxt.getText());
					int c = Integer.valueOf(cIdtxt.getText());
					int s = Integer.valueOf(sIdtxt.getText());

					// send to Kalos's method for check
					// object.method(fN, lN, dOB, aS, p, em, a, c, s);
				}
				if(target == archB){
					int id = Integer.valueOf(cIdtxt.getText());
					// send to Kalos's method for check
					// object.method(id);
				}
			}
		};
		

		/************************************************************************************************************************************/
		/********************************************* SUBSCRIPTION CARD ********************************************************************/

		subscriptions.setBorder(BorderFactory.createTitledBorder("  Subscription Details   "));
		subscriptions.setBackground(new Color(100, 149, 237));
		JPanel subsPanelL = new JPanel();
		JPanel subsPanelR = new JPanel();
		JPanel subsPanelTOP = new JPanel();
		JPanel subsPanelD = new JPanel();	
		JPanel subsPanelW = new JPanel();
		JPanel subsPanelM = new JPanel();
		subsPanelTOP.setBorder(BorderFactory.createTitledBorder("Type Of Publication"));		
		subscriptions.add(subsPanelTOP);
		subsPanelTOP.add(subsPanelD);
		subsPanelTOP.add(subsPanelW);
		subsPanelTOP.add(subsPanelM);
		subsPanelTOP.setBackground(new Color(100, 149, 237));
		subsPanelL.setBackground(new Color(100, 149, 237));
		subsPanelL.setLayout(new GridLayout(8, 1, 0, 5));
		subsPanelL.setBorder(BorderFactory.createEmptyBorder(20, 80, 0, 0));		
		subscriptions.add(subsPanelL);
		subsPanelR.setBackground(new Color(100, 149, 237));
		subsPanelR.setLayout(new GridLayout(8, 1, 0, 5));
		subsPanelR.setBorder(BorderFactory.createEmptyBorder(20, 80, 0, 0));
		subscriptions.add(subsPanelR);		
		subsPanelD.setBackground(new Color(100, 149, 237));
		subsPanelD.setLayout(new GridLayout(8, 1, 0, 5));
		subsPanelD.setBorder(BorderFactory.createEmptyBorder(20, 80, 0, 0));		
		subsPanelW.setBackground(new Color(100, 149, 237));
		subsPanelW.setLayout(new GridLayout(8, 1, 0, 5));
		subsPanelW.setBorder(BorderFactory.createEmptyBorder(20, 80, 0, 0));		
		subsPanelM.setBackground(new Color(100, 149, 237));
		subsPanelM.setLayout(new GridLayout(8, 1, 0, 5));
		subsPanelM.setBorder(BorderFactory.createEmptyBorder(20, 80, 0, 0));		
		
		JLabel lblDaily = new JLabel("Daily");
		lblDaily.setForeground(new Color(255, 0, 0));
		subsPanelD.add(lblDaily);
		JRadioButton rdbtnNewsPaper1 = new JRadioButton("Newspaper1");
		subsPanelD.add(rdbtnNewsPaper1);
		rdbtnNewsPaper1.setBackground(new Color(100, 149, 237));		
		JRadioButton rdbtnNewsPaper2 = new JRadioButton("Newspaper2");
		subsPanelD.add(rdbtnNewsPaper2);
		rdbtnNewsPaper2.setBackground(new Color(100, 149, 237));		
		JRadioButton rdbtnNewsPaper3 = new JRadioButton("Newspaper3");
		subsPanelD.add(rdbtnNewsPaper3);
		rdbtnNewsPaper3.setBackground(new Color(100, 149, 237));	
		JRadioButton rdbtnNewsPaper4 = new JRadioButton("Newspaper4");
		subsPanelD.add(rdbtnNewsPaper4);
		rdbtnNewsPaper4.setBackground(new Color(100, 149, 237));		
		JRadioButton rdbtnNewsPaper5 = new JRadioButton("Newspaper5");
		subsPanelD.add(rdbtnNewsPaper5);
		rdbtnNewsPaper5.setBackground(new Color(100, 149, 237));
				
		JLabel lblWeekly = new JLabel("Weekly");
		lblWeekly.setForeground(new Color(255, 0, 0));
		subsPanelW.add(lblWeekly);
		JRadioButton rdbtnMagazine1 = new JRadioButton("Magazine1");
		subsPanelW.add(rdbtnMagazine1);
		rdbtnMagazine1.setBackground(new Color(100, 149, 237));		
		JRadioButton rdbtnMagazine2 = new JRadioButton("Magazine2");
		subsPanelW.add(rdbtnMagazine2);
		rdbtnMagazine2.setBackground(new Color(100, 149, 237));		
		JRadioButton rdbtnMagazine3 = new JRadioButton("Magazine3");
		subsPanelW.add(rdbtnMagazine3);
		rdbtnMagazine3.setBackground(new Color(100, 149, 237));	
		JRadioButton rdbtnMagazine4 = new JRadioButton("Magazine4");
		subsPanelW.add(rdbtnMagazine4);
		rdbtnMagazine4.setBackground(new Color(100, 149, 237));		
		JRadioButton rdbtnMagazine5 = new JRadioButton("Magazine5");
		subsPanelW.add(rdbtnMagazine5);
		rdbtnMagazine5.setBackground(new Color(100, 149, 237));
		//Test
		
		JLabel lblMonthly = new JLabel("Monthly");
		lblMonthly.setForeground(new Color(255, 0, 0));
		subsPanelM.add(lblMonthly);
		JRadioButton rdbtnMagazineM1 = new JRadioButton("Magazine M1");
		subsPanelM.add(rdbtnMagazineM1);
		rdbtnMagazineM1.setBackground(new Color(100, 149, 237));		
		JRadioButton rdbtnMagazineM2 = new JRadioButton("Magazine M2");
		subsPanelM.add(rdbtnMagazineM2);
		rdbtnMagazineM2.setBackground(new Color(100, 149, 237));		
		JRadioButton rdbtnMagazineM3 = new JRadioButton("Magazine M3");
		subsPanelM.add(rdbtnMagazineM3);
		rdbtnMagazineM3.setBackground(new Color(100, 149, 237));
		
		JLabel lblCustomerId = new JLabel("Customer ID");
		subsPanelL.add(lblCustomerId);		
		JTextField txtCustomerID = new JTextField();
		subsPanelL.add(txtCustomerID);		
		JLabel lblTotal = new JLabel("Total ");
		subsPanelL.add(lblTotal);		
		JTextField textTotal = new JTextField();
		subsPanelL.add(textTotal);		
		JButton btnSubscribe = new JButton("Subscribe");
		subscriptions.add(btnSubscribe);	
		JButton btnRenew = new JButton("Renew");
		subscriptions.add(btnRenew);
		JButton btnModify = new JButton("Modify");
		subscriptions.add(btnModify);
		JButton btnSearch = new JButton("Search");
		subscriptions.add(btnSearch);
		
		creB.addActionListener(CustomerButtonsEvent);
		modB.addActionListener(CustomerButtonsEvent);
		searchB.addActionListener(CustomerButtonsEvent);
		archB.addActionListener(CustomerButtonsEvent);		

		mainPanel.add(top, BorderLayout.NORTH);
		mainPanel.add(mid, BorderLayout.CENTER);
		mainPanel.add(bot, BorderLayout.SOUTH);
		mainPanel.add(left, BorderLayout.WEST);
		mainPanel.add(right, BorderLayout.EAST);

		cp.add(mainPanel);
		frame.setSize(900, 750);
		frame.setVisible(true);
	}
	
	/*
	
	public void createCustomer(String answer){
		
	}
	
	public void connectToCustomer(Customer customer){
		this.customer = customer;
	}
	
	public void connectToDB(systemDB DB){
		this.DB = DB;
	}
*/
}

